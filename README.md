# 3DCloud-HyperSpectralFusion

This programs is use to fuse a 3D point cloud with hyperspectral images (HSI).  Point Cloud can be obtained via LiDAR, a laser scanner,  structure from motion...  Raster image may be a RGB file or may contain any number of bands. The result will be a point cloud with new fields containing the HSI data and a raster containing the DEM made from the cloud. This .tif file is registered to the HSI file (having the same CRS, image size, pixel size and pixel posting).  Raster DEM contains 8 bands:


- band 1 = n cloud point count
- band 2 = min z
- band 3 = max z
- band 4 = sum z
- band 5 = average z
- band 6 = nodata mask from input raster.
- band 7 = nodata mask from point cloud.
- band 8 = mask intersection    

Kalacska M., Arroyo-Mora J.P., Cadieux N., Inamdar D., Lucanus O. UAV Hyperspectral-3D fusion for peatland biogeochemistry. 11th EARSeL SIG Imaging Spectroscopy Workshop. Brno, Czech Republic. Feb 2019.

Cadieux N., Inamdar D., Kalacska M., Arroyo-Mora J.P., Lucanus O. HSI_3DCloud_Fusion.py : An OpenSource Hyperspectral Imagery – 3D Point Cloud Fusion Program. Exploring the n Dimensions of Archaeology, L’archéologie sous toutes ses dimensions. Conjoint Annual Meetings of the Canadian Archaeological Association and the Association des archéologues de Québec. Québec, Québec. May 2019.