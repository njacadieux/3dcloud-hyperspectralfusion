# -*- coding: utf-8 -*-

"""
Name hsi_3d_fusion_v1.0

This programs is use to fuse a point cloud with Hyperspectral or RGB data.
Use .csv with no header for input cloud.Output will be input csv plus output
bands.No header is added to csv. Input files driver are from gdal.  In theory,
all gdal formats are accepted. This remains to be tested!

Warning: Both files need to be in the same CRS.

Python Version 3.7


@author: Nicolas Cadieux
Copyright (C) <2019> <Nicolas Cadieux>
GPL-3.0-or-later

This program is free software: you can redistribute it and/or modify it under
the terms of theGNU General Public License as published by the Free Software
Foundation, either version 3 of theLicense, or (at your option) any later
version.This program is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.You should have received a copy of the GNU
General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.


Help for this code was found on the following web pages.
https://gis.stackexchange.com/questions/46893/getting-pixel-value-of-gdal-raster-under-ogr-point-without-numpy
https://pcjericks.github.io/py-gdalogr-cookbook/raster_layers.html
https://pdal.io/python.html#python
https://jakevdp.github.io/PythonDataScienceHandbook/02.01-understanding-data-types.html
https://gdal.org/python/

NEW FEATURES, FIXES AND KNOW ISSUES
*******NEW FEATURES
    v 1.0
    -Simple Scaling of data for better visualization in CloudCompare. You will
    need to adjust intensities of RGB with Edit/Colors/Levels as data
    is not centered.
    v 2.0
    -Works with geotransforms with image rotation.
    -Dropped python 2.7 version of code.
    v 3.0
    -batch_it_v010_python36.py will need to be updated to work with v 3.
    -Creates a raster DEM containing the same dimensions as the input raster.
    -Now uses numba to speed up the code

*******FIXES
fixed in 1.0:
    -Crashes if band does not exit in raster. Additional checks are made before
    fusion starts.
    -Refactoring of variables for better code clarity
    -Additional error checks made for user input variable.
    -Header not written when file OUTPUT_FILE_WRITE_MODE == "w"

*******Know issues:
    -v 2.0
    -Image rotation is handled but your point cloud must also be rotated.
    -Image and point cloud must be in the same CRS. No checks done!
    -v 3.0
    -batch_it_v010_python36.py will need to be updated to work with v 3.
"""

import argparse
import struct
import os
import time
import sys
import multiprocessing
from multiprocessing import Pool
from itertools import zip_longest
from osgeo import gdal, osr
import numpy as np
from numba import jit

# from osgeo import ogr
# from sys import getsizeof

START_TIME = time.time()

# =============================================================================
#  User Input Variables
# =============================================================================

# 1) File Input and  Output Variables
# Make your (MY) life simple and don't use spaces and funny character
# in the path!  DO NOT REMOVE the r
CLOUD_FILE_STR = os.path.abspath(r"C:\temp\Cloud.csv")
HSI_RASTER_STR = os.path.abspath(r"C:\temp\180922_WTE3_EPSG_NODATA0.tif")
OUTPUT_CLOUD_FILE_STR = os.path.abspath(r'C:\temp\output_Cloud.csv')
OUTPUT_RASTER_FILE_STR = os.path.abspath(r'C:\temp\output_raster.tif')

# =============================================================================
#   File protections and write modes:
#   PROTECT_OUTPUT_FILES
#      True of False: If output files exist,this will stop the script.
#   OUTPUT_FILE_WRITE_MODE = 'w'
#       ex: 'a' or 'w' a: the results will be added at the end of the file.
#       'w' file will be overwritten.
# =============================================================================

PROTECT_OUTPUT_FILES = True
OUTPUT_FILE_WRITE_MODE = 'w'

# =============================================================================
#  Input an output csv formats
# Cloud file csv separator
# ex:
# ',' == comma
# ' ' == space
# ';' == semicolon
# '\t' == tab
# =============================================================================
CLOUD_FILE_CSV_SEPARATOR = ','
OUTPUT_FILE_CSV_SEPARATOR = ','
# =============================================================================
# 2) File Headers and Data Fields
# =============================================================================

CLOUD_FILE_HEADER_STR = "x,y,z,r,g,b"  # This also indicate the x and y fields
BAND_HEADER_PREFIX = 'band_'  # ex: 'band_'= band_1, band_2
WRITE_HEADER = True  # True or False.  Write Header to output file.
# Number of lines to skip if you have a headers in your input cloud files.
LINE2SKIP_IN_CLOUD_FILE = 0

# =============================================================================
#  3) Input Field Order, Bands to Query and Values to Skip
# Use python nested list format. Bands will be placed in order and duplicate
# values will be eliminated. Band indexing starts at 1 and not 0.
# ex: [1,2,3,4] == [4,1,3,2]
# ex: [1,2,3,4,1] == [1,2,3,4]
# ex: [1,2,3,4] == [[1,4]] (or first and last band in the file) Use double
# bracket if no other value are selected.  [[1,4]] == bands 1,2,3,4 and
# [1, 4] == only band 1 and band 4.
# ex: [1,2,3,5] == [[1,3],5]
# ex: [1,[6,8]] == band 1,6,7,8
# =============================================================================

CLOUD_FILE_FIELDS2KEEP_STR = "x,y,z,r,g,b"  # or "x,y,z"(use quotes)
BANDS2KEEP_USER = [1, 2, [3, 5]]  # no band 0 in gdal

# =============================================================================
# Input any raster values you want to skip in the cloud.
# Use same format as BANDS2KEEP_USER
# =============================================================================

RASTER_VAL2SKIP_USER = [0]

# =============================================================================
# Rescalling raster data between 0 and RESCALEMAX for the cloud data.
# =============================================================================

RESCALEDATA = False
RESCALE2INT = False  # True for int, False for float
RASTERMAXVALUE = 32768  # ex: 65535
RESCALEMAX = 1  # ex: 255.  Data will be scalled between 0 and 255

# =============================================================================
# OUTPUT_RASTER_DEM
# =============================================================================

OUTPUT_RASTER_NODATA_VAL = -32768
OUTPUT_RASTER_DTYPE = 'Float32'  # Or Float64. DO NOT USE ANY INT!!!!
HSI_RASTER_MASK_VALUES = [0, -32768]  # Theses value will = 1 in masks, others

# =============================================================================
#  4) System Optimization
# EX:
# THREADS = 12   Number of cpu cores you want to use.
# N_POINTS = 10000  Number of points to send to threads.
# WRITE_CYCLE_MULTIPLIER = 10  Number of N_POINTS to be
# kept in memory before writing to file.
#   This will be mulitplied by the number of THREADS. Playing with could speed
#   up the script. Lowering this number will decrease memory use. Do not put 0.
# =============================================================================

THREADS = multiprocessing.cpu_count()
N_POINTS = 10000  # 1 000 to 10 000
WRITE_CYCLE_MULTIPLIER = 10  # 1 to 10

# =============================================================================
#  *********************** USER Input variable END ****************************
# =============================================================================


def create_directory(path):
    """Create a directory. Will pass if directory has been added
      by another tread."""
    err = ''
    if os.path.isdir(path):
        pass
    else:
        try:
            os.makedirs(path)
        except WindowsError as err:
            pass
    return err


def cnt_cpu():
    """Count CPU'S and inform user."""
    cpu_cnt = multiprocessing.cpu_count()
    print('System has '+str(cpu_cnt)+' cores. '
          'You are using '+str(THREADS)+'.')
    return cpu_cnt


def open_csv_file(filestr):
    """todo"""
    try:
        vector_layer = open(filestr, 'r')
        message = 'Open csv file successful: ' + filestr
    except IOError:
        err = r'Cannot open vector file: ' + filestr
        vector_layer.close()
        sys.exit(err)
    return vector_layer, message


def cloud_field_filter(field2keep, header_dct):
    """Make a list of csv positions to keep for input cloud file."""
    cloud_fields2keep_lst = []
    for x in field2keep.split(','):
        try:
            cloud_fields2keep_lst.append(header_dct[x])
        except KeyError:
            print('Check CLOUD_FILE_HEADER_STR and CLOUD_FILE_HEADER_STR ',
                  ' variable', x,
                  ' does not exits in Check CLOUD_FILE_HEADER_STR')
            pass
    return cloud_fields2keep_lst


def mk_cloud_header_dct(headerstr):
    """Make a dict with the CLOUD_FILE_HEADER_STR variable."""
    header_dct = {}
    for lineo, field in enumerate(headerstr.split(',')):
        if field in header_dct:
            err = 'Duplicate header field value:', field
            sys.exit(err)
            pass
        else:
            header_dct.update({field: lineo})
    return header_dct


def mk_cloud_file_header(cloud_file_fields2keep_str, bands2query_lst):
    """Make the output file header."""
    output_lst = []

    for x in cloud_file_fields2keep_str.split(','):
        output_lst.append(x)

    for x in bands2query_lst:
        output_lst.append(BAND_HEADER_PREFIX+str(x))
    return output_lst


def check_cloud_header(filestr, headerstr):
    """Makes sure the number of field specified by user == to the field in the
    cloud file header. Prints first line of data."""
    header_dct = mk_cloud_header_dct(headerstr)
    err = 0
    cloud_layer, message = open_csv_file(filestr)
    headerlen = len(header_dct)
    messagelst = []
    messagelst.append(
        'Cloud file has ' + str(headerlen) + ' fields: ' + str(headerstr))
    for skips in range(0, LINE2SKIP_IN_CLOUD_FILE):
        skips = cloud_layer.readline()
        messagelst.append(
            'Skipping cloud file header lines:' + skips.replace('\n', ''))

    firstline = cloud_layer.readline()
    messagelst.append(
        'This should be your first line of data: ' + str(firstline))
    firstline_fieldcnt = len(firstline.split(CLOUD_FILE_CSV_SEPARATOR))
    if firstline_fieldcnt == headerlen:
        pass
    else:
        messagelst.append(
            'ERROR: You indicate your data has \
            '+str(headerlen)+' fields but your '
            'first line of data has ' + str(
                firstline_fieldcnt) + ' fields')
        cloud_layer.close()
        err = 1
        return messagelst, err
    cloud_layer.close()
    return messagelst, err


def output_is_a_file(filestr: str):
    """
    Stop from overwriting existing files.

    Parameters
    ----------
    filestr : str
        DESCRIPTION.

    Returns
    -------
    message : TYPE
        DESCRIPTION.
    err : TYPE
        DESCRIPTION.

    """

    if os.path.isfile(filestr):
        err = 1
        message = 'Output file '+filestr+' exists.'
    else:
        err = 0
        message = 'Output file '+filestr+' does not exist'
    return message, err


def create_output_file(filestr):
    """Create output directory if not present. FOR NOW
    IN APPEND MODE"""
    create_directory(os.path.dirname(filestr))
    output_file = open(filestr, OUTPUT_FILE_WRITE_MODE)
    return output_file


def open_raster_file(filestr):
    """Open raster dataset"""
    raster_ds = gdal.Open(filestr)
    message = 'Open raster dataset ' + filestr
    if raster_ds is None:
        message = ''
        sys.exit('Unable to open ' + filestr)
    return raster_ds, message


def check_raster_bands(hsi_raster_str):
    """Open raster file and loop bands to check NODATA and DataType to make
       sure file bands have identical NODATA AND Data type values.
       SYS.EXIT If problems are found """
    raster_ds = (open_raster_file(hsi_raster_str)[0])
    nodatavalue_dct = {}
    data_type_dct = {}
    rb_cnt = raster_ds.RasterCount
    invalide_rb_lts = []
    band_report_message = []

    for band in range(rb_cnt):
        band += 1
        raster_band = raster_ds.GetRasterBand(band)
        if raster_band is None:
            print('Raster band is None. Skipping', band)
            invalide_rb_lts.append(band)
            continue
        nodata = raster_band.GetNoDataValue()
        rb_data_type = gdal.GetDataTypeName(raster_band.DataType)
        band_report_message.append(
            str(band) + ',' + str(nodata) + ',' + str(rb_data_type))
        nodatavalue_dct.update({band: nodata})
        data_type_dct.update({band: rb_data_type})

    if invalide_rb_lts:
        print('The following band cannot be read by the GDAL driver:\n')
        for err in invalide_rb_lts:
            print(err)
            print('\n')
    else:
        pass
    raster_ds = None
    return invalide_rb_lts, nodatavalue_dct,\
        data_type_dct, rb_cnt, band_report_message


def sort_bands2keep():
    """ Sort raster bands wanted by user. Eliminate bands that cannot be read
    by GDAL or band that do not exit (ex: user asked for band 1000
    but raster has only 999 bands. USES GLOBAL VARIABLES"""
    bands2keep_lts = []
    try:
        for index in BANDS2KEEP_USER:
            if isinstance(index, int):
                if index not in bands2keep_lts:
                    # check if band number exits in raster
                    if index >= 1 and index <= RB_CNT:
                        bands2keep_lts.append(index)
                    else:
                        print('Band', index, 'does not exit in your raster.')

            elif isinstance(bands2keep_lts, list):
                brange = list(range(index[0], (index[1])+1))
                for val in brange:
                    if val not in bands2keep_lts:
                        if val >= 1 and val <= RB_CNT:
                            bands2keep_lts.append(val)
                        else:
                            print('Band', val, 'does not exit in your raster.')
            else:
                print('Please use python nest lists format with \
                positive intergers only')
                print('Please see https://docs.python.org/2/tutorial\
                /introduction.html#lists')
                pass
    except:
        print('Please use python list of list format with positive\
        intergers only')
        print('Please see https://docs.python.org/2/tutorial/\
        introduction.html#lists')
        sys.exit()

    bands2keep_lts.sort()
    for val in INVALIDE_RB_LTS:
        if val in bands2keep_lts:
            bands2keep_lts.remove(val)
        else:
            pass

    print('The following bands are being processed', bands2keep_lts)
    return bands2keep_lts


def check_geotransform(raster_ds):
    """Check geotransform before algo starts. """
    # raster_ds = (open_raster_file(hsi_raster_str)[0])
    raster_ds_geotransform = raster_ds.GetGeoTransform()
    # INV_GT = gdal.InvGeoTransform(RASTER_DS_GEOTRANSFORM)
    inv_gt = gdal.InvGeoTransform(raster_ds_geotransform)
    is_rotated = False
    # print raster_ds_geotransform
    if raster_ds_geotransform == (0.0, 1.0, 0.0, 0.0, 0.0, 1.0):
        print('No geotransform found!. Are your file georeferenced?')
    else:
        pass

    if raster_ds_geotransform[2] == 0:
        pass
    else:
        is_rotated = True
    if raster_ds_geotransform[4] == 0:
        pass
    else:
        is_rotated = True
    raster_ds = None
    return raster_ds_geotransform, inv_gt, is_rotated


def sort_values2keep(raster_val2skip):
    """ Sort raster bands values 2 skip."""
    raster_val2skip_lts = []
    try:
        for index in raster_val2skip:
            if isinstance(index, int):
                if index not in raster_val2skip_lts:
                    raster_val2skip_lts.append(index)
            elif isinstance(raster_val2skip_lts, list):
                brange = list(range(index[0], (index[1])+1))
                for val in brange:
                    if val not in raster_val2skip_lts:
                        raster_val2skip_lts.append(val)
            else:
                print('Please use python nest lists format with \
                positive intergers only')
                print('Please see https://docs.python.org/2/tutorial\
                /introduction.html#lists')
                pass
    except:
        print('Please use python list of list format with positive\
        intergers only')
        print('Please see https://docs.python.org/2/tutorial/\
        introduction.html#lists')
        sys.exit()

    message = 'The following values will be skiped', raster_val2skip_lts
    return raster_val2skip_lts, message

def grouper(iterable, chunksize, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx
    args = [iter(iterable)] * chunksize
    return zip_longest(fillvalue=fillvalue, *args)


def check_input_output_files(
        inputcsv: str, input_hsi_raster: str,
        outputcsv: str, output_raster: str):
    """
    Check all input and output file before we start.

    Parameters
    ----------
    inputcsv : str
        DESCRIPTION.
    input_hsi_raster : str
        DESCRIPTION.
    outputcsv : str
        DESCRIPTION.
    output_raster : str
        DESCRIPTION.

    Returns
    -------
    None.

    """
    print("******************************************************************")
    print("**********Checking input files and output files.*********")
    print("******************************************************************")
    # Open and close all Cloud layer
    f, message = open_csv_file(inputcsv)
    print(message)
    f.close()
    # Check Headers
    messages, err = check_cloud_header(inputcsv, CLOUD_FILE_HEADER_STR)
    for m in messages:
        print(m)
    if err == 1:
        sys.exit('Error!!! Problem with header.')
    # Open and checks all raster datasets and band
    print(open_raster_file(input_hsi_raster)[1])
    print('Band number, band nodata, band data type: ')
    message = (check_raster_bands(input_hsi_raster)[4])
    for ms in message:
        print(ms)

    # Next line does not work since input have been changed from text string
    # to a band.
    # print('File geotransform:', check_geotransform(input_hsi_raster))
    message, err = output_is_a_file(outputcsv)
    print(message)
    message, err = output_is_a_file(output_raster)
    print(message)
    print('\n')
    print("******************************************************************")
    print("***************** Files look good :) ************************")
    print("******************************************************************")
    print('\n')


def scale_data(val):
    """Scales the HSI DATA"""

    if RESCALE2INT is True:
        scaled_val = ((val) * RESCALEMAX)/RASTERMAXVALUE
        return int(round(scaled_val))
    elif RESCALE2INT is False:
        scaled_val = (float(val)*float(RESCALEMAX))/float(RASTERMAXVALUE)
        return scaled_val


def hsi_3d_fusion(cloud_lst):
    """Use Gdal to query_raster. Both files must be georeferenced and must
       have the same CRS."""
    listout = []
    for line in cloud_lst:
        # Jump Grouper None lines
        if line is None:
            pass
        else:
            # remove end of line character from non None lines.
            line = line.replace('\n', '')  # TODO Use .remove?
            cloud_fields = []
            # Write selected cloud field
            map_x = float(line.split(',')[CLOUD_HEADER_DCT['x']])
            map_y = float(line.split(',')[CLOUD_HEADER_DCT['y']])

            for cfield in CLOUD_FIELD2KEEP_LST_LINESPLIT_INDEX:
                cloud_fields.append(line.split(',')[cfield])

            for band in ORDERED_BANDS2QUERY_LST:
                raster_band = RASTER_DS.GetRasterBand(band)
                # Convert from map to pixel coordinates.
                if RASTER_IS_ROTATED is False:
                    pixel_x = int((map_x - RASTER_DS_GEOTRANSFORM[0]) /
                                  RASTER_DS_GEOTRANSFORM[1])  # x pixel
                    pixel_y = int((map_y - RASTER_DS_GEOTRANSFORM[3]) /
                                  RASTER_DS_GEOTRANSFORM[5])  # y pixel
                # Convert from map to pixel coordinates with raster rotation
                elif RASTER_IS_ROTATED is True:
                    pixel_x = int(
                        INV_GT[0] + map_x * INV_GT[1] + map_y * INV_GT[2])
                    pixel_y = int(
                        INV_GT[3] + map_x * INV_GT[4] + map_y * INV_GT[5])

                structval = raster_band.ReadRaster(
                    pixel_x, pixel_y, 1, 1, 1, 1, raster_band.DataType)
                # https://www.gdal.org/gdallocationinfo.html

# =============================================================================
# Conditions for not writing line in outputfile
# =============================================================================
#               If pixel fall outside of raster
                if not structval:
                    break
#               If any value has a raster nodata value
                elif (
                        struct.unpack(FMTTYPES[gdal.GetDataTypeName(
                        raster_band.DataType)] * 1, structval)[0]
                        ) == NODATAVALUE_DCT[band]:
                    break
#               If any pixel value is found in the RASTER_VAL2SKIP variable.
                elif (
                        struct.unpack(FMTTYPES[gdal.GetDataTypeName(
                        raster_band.DataType)] * 1, structval)[0]
                        ) in RASTER_VAL2SKIP:
                    break
                else:
                    if RESCALEDATA is True:
                        # testing with float should not be a float
                        # val=(float(struct.unpack(FMTTYPES[gdal.GetDataTypeName(raster_band.DataType)] * 1, structval)[0]))#if scaling is 0 to 1

                        val = (struct.unpack(FMTTYPES[gdal.GetDataTypeName(
                            raster_band.DataType)] * 1, structval)[0])
                        val = scale_data(val)
                        cloud_fields.append(str(val))
                    else:
                        cloud_fields.append(str(
                            struct.unpack(FMTTYPES[gdal.GetDataTypeName
                                                   (raster_band.DataType
                                                    )] * 1, structval)[0]))

#           Else belongs to the for loop. Will be executed unless condition
#           stops with a break
            else:
                n = 0
                for final in cloud_fields:
                    n += 1
                    listout.append(final)
                    # make csv, skip empty lines
                    if n < len(cloud_fields):
                        listout.append(OUTPUT_FILE_CSV_SEPARATOR)
                    elif n == len(cloud_fields):
                        listout.append('\n')
    return listout

@jit(nopython=True)
def query_speed_up(slice_arr, map_z):

    if slice_arr[0] == 0:
        slice_arr[0] = 1
    else:
        slice_arr[0] = slice_arr[0] + 1

    # min z
    if np.isnan(slice_arr[1]) == True:  # not is
        slice_arr[1] = map_z
    elif slice_arr[1] > map_z:
        slice_arr[1] = map_z
    else:
        pass

    # max z
    if np.isnan(slice_arr[2]) == True:  # not is
        slice_arr[2] = map_z
    elif slice_arr[2] < map_z:
        slice_arr[2] = map_z
    else:
        pass

    # sum z
    if np.isnan(slice_arr[3]) == True:  # not is
        slice_arr[3] = map_z
    else:
        slice_arr[3] = slice_arr[3] + map_z
    return slice_arr


def rasterize(cloud_file_str: str, cloud_header_dct: str,
              line2skip_in_cloud_file: str, hsi_raster_str: str,
              output_raster_file_str: str, output_raster_nodata_val: str,
              hsi_raster_mask_values: str):
    """

    Parameters
    ----------
    cloud_file_str : str
        DESCRIPTION.
    cloud_header_dct : str
        DESCRIPTION.
    line2skip_in_cloud_file : str
        DESCRIPTION.
    hsi_raster_str : str
        DESCRIPTION.
    output_raster_file_str : str
        DESCRIPTION.
    output_raster_nodata_val : str
        DESCRIPTION.
    hsi_raster_mask_values : str
        DESCRIPTION.

    Returns
    -------
    None.

    """

    print("Rasterizing Point Cloud to DEM...", '\n')

# =============================================================================
# Open cloud file and read header lines.  Do nothing with header.
# =============================================================================

    cloud_layer, MESSAGE = open_csv_file(cloud_file_str)
    print('Skipping Header lines...')
    for skips in range(0, line2skip_in_cloud_file):
        skips = cloud_layer.readline()
        print(skips)

# =============================================================================
# Open input HSI dataset get SpatialReference, Geotransform.  Will apply to
# output raster.
# =============================================================================

    nbands = 8
    raster_ds = open_raster_file(hsi_raster_str)[0]
    raster_ds_geotransform, inv_gt, raster_is_rotated = \
        check_geotransform(raster_ds)
    raster_srs = osr.SpatialReference()
    raster_srs.ImportFromWkt(raster_ds.GetProjectionRef())
    raster_ds_ncol = (raster_ds.RasterXSize)
    raster_ds_nrow = (raster_ds.RasterYSize)
    mask = gdal.Band.ReadAsArray(raster_ds.GetRasterBand(1))
    # Create array
    array = np.full((nbands, raster_ds_nrow, raster_ds_ncol),
                    np.nan, dtype=OUTPUT_RASTER_DTYPE)
    print('Output file array dtype =', array.dtype)
    driver = gdal.GetDriverByName('GTiff')
    target_ds = driver.Create(output_raster_file_str,
                              xsize=raster_ds_ncol,
                              ysize=raster_ds_nrow,
                              bands=nbands, eType=gdal.GDT_Float32)

# =============================================================================
# Read cloud and build arrays
# array 0, band 1 = n cloud point count
# array 1, band 2 = min z
# array 2, band 3 = max z
# array 3, band 4 = sum z
# array 4, band 5 = average z
# array 5, band 6 = nodata mask from input raster.
# array 6, band 7 = nodata mask from point cloud.
# array 7, band 8 = mask difference
# =============================================================================
#    hsi_raster_mask_values

    condition_list = []
    for c in hsi_raster_mask_values:
        condition_list.append(mask == c)
    choice_list = [0]*len(condition_list)
    default_val = 1
    # array 5 = raster no data mask
    array[5] = np.select(condition_list, choice_list, default_val)
    mask = None
    raster_ds = None  # close gdal dataset
    # array [0] = number of observations, set to 0.
    array[0] = 0.0

#   Convert file to list if hard drive is slow.
#    cloud_layer = list(cloud_layer) # Comment this out if running out of memory

    for line in cloud_layer:
        map_x = float(line.split(',')[cloud_header_dct['x']])
        map_y = float(line.split(',')[cloud_header_dct['y']])
        map_z = float(line.split(',')[cloud_header_dct['z']])
        pixel_x = int(inv_gt[0] + map_x * inv_gt[1] + map_y * inv_gt[2])
        pixel_y = int(inv_gt[3] + map_x * inv_gt[4] + map_y * inv_gt[5])

        # skip point if cloud is outside of raster
        if pixel_x < 0 or pixel_x > raster_ds_ncol:
            continue
        if pixel_y < 0 or pixel_y > raster_ds_nrow:
            continue
        # slice_arr is created using numba.  Much faster.
        slice_arr = array[0:4, pixel_y, pixel_x]  # Take a slice of the array
        array[0:4, pixel_y, pixel_x] = query_speed_up(slice_arr, map_z)
    # array 4, average
    array[4] = array[3] / array[0]
    # array 6, cloud mask
    array[6] = np.select([array[0] > 0], [1], 0)
    # array 7, mask intersection
    array[7] = array[6]*array[5]

# =============================================================================
# Set no data, band description, band full stats, write file, close file.
# =============================================================================

# =============================================================================
#
#     values = [ ('TIFFTAG_DOCUMENTNAME'    , 'document_name'),
#                ('TIFFTAG_IMAGEDESCRIPTION', 'image_description'),
#                ('TIFFTAG_SOFTWARE'        , 'software'),
#                ('TIFFTAG_DATETIME'        , '2009/01/01 13:01:08'),
#                ('TIFFTAG_ARTIST'          , 'artitst'),
#                ('TIFFTAG_HOSTCOMPUTER'    , 'host_computer'),
#                ('TIFFTAG_COPYRIGHT'       , 'copyright'),
#                ('TIFFTAG_XRESOLUTION'     , '100'),
#                ('TIFFTAG_YRESOLUTION'     , '101'),
#                ('TIFFTAG_RESOLUTIONUNIT'  , '2 (pixels/inch)'),
#                ('TIFFTAG_MINSAMPLEVALUE'  , '1' ),
#                ('TIFFTAG_MAXSAMPLEVALUE'  , '2' ),
#              ]
# =============================================================================

    metadata = [('TIFFTAG_IMAGEDESCRIPTION', 'Fusion of HSI and point cloud.'),
                ('TIFFTAG_SOFTWARE', 'hsi_3d_fusion.py'),
                ('TIFFTAG_COPYRIGHT', 'please visit https://gitlab.com/\
                 njacadieux/3dcloud-hyperspectralfusion')]
    dict = {}
    for item in metadata:
        dict[item[0]] = item[1]
    target_ds.SetMetadata(dict)

    target_ds.SetGeoTransform(raster_ds_geotransform)
    target_ds.SetProjection(raster_srs.ExportToWkt())
    # Set no value to dataset.  Tiff = only one nodata value per dataset.
    band = target_ds.GetRasterBand(1)
    band.SetNoDataValue(output_raster_nodata_val)
    band_description = ['n cloud points', 'min z', 'max z', 'sum z',
                        'avg z', 'hsi nodata mask', 'cloud nodata mask',
                        'mask intersection']

    for b, bdesc in zip(range(1, nbands + 1), band_description):
        band = target_ds.GetRasterBand(b)
        band.SetDescription(bdesc)

    for b in range(0, nbands, 1):
        target_ds.GetRasterBand(b+1).WriteArray(array[b])
        approx_stats = False
        target_ds.GetRasterBand(b+1).ComputeStatistics(approx_stats)
        min, max, mean, stdev = target_ds.GetRasterBand(b+1).GetStatistics(
            approx_stats, True)
        target_ds.GetRasterBand(b+1).SetStatistics(min, max, mean, stdev)

    target_ds = None

    try:
        cloud_layer.close()  # will fail if file converted to a list
    except AttributeError:
        cloud_layer = []

    return 'Rasterizing done'

# =============================================================================
#  Command line arguments
# =============================================================================
# TODO test with output_raster must add new arguments to file


parser = argparse.ArgumentParser()
parser.add_argument('-csv', '--inputcsv', default=None)
parser.add_argument('-raster', '--inputraster', default=None)
parser.add_argument('-output', '--outputcsv', default=None)
parser.add_argument('-output_raster', '--output_raster', default=None)
args = parser.parse_args()
print(args)

# TODO better parsing of input arguments.  This sucks!
if args.inputcsv is None and args.inputraster is None and args.outputcsv is None and args.output_raster is None:
    # Check files for most errors IF not called from command line.
    check_input_output_files(
        CLOUD_FILE_STR, HSI_RASTER_STR,
        OUTPUT_CLOUD_FILE_STR, OUTPUT_RASTER_FILE_STR)
    pass
else:
    CLOUD_FILE_STR = os.path.abspath(args.inputcsv)
    HSI_RASTER_STR = os.path.abspath(args.inputraster)
    OUTPUT_CLOUD_FILE_STR = os.path.abspath(args.outputcsv)
    OUTPUT_RASTER_FILE_STR = os.path.abspath(args.output_raster)

# =============================================================================
#  Open files
# =============================================================================
cnt_cpu()  # Count cpus, prints out results.
CLOUD_LAYER, MESSAGE = open_csv_file(CLOUD_FILE_STR)
RASTER_DS = open_raster_file(HSI_RASTER_STR)[0]
# =============================================================================
#   MAKE GLOBAL VARIABLES AND CHECK RASTER
# =============================================================================
# Dictionary for raster data types

FMTTYPES = {
    'Byte': 'B', 'UInt16': 'H', 'Int16': 'h', 'UInt32': 'I', 'Int32': 'i',
    'Float32': 'f', 'Float64': 'd'}

RASTER_DS_GEOTRANSFORM, INV_GT, RASTER_IS_ROTATED = check_geotransform(
    RASTER_DS)

# CHECK RASTER
INVALIDE_RB_LTS, NODATAVALUE_DCT, DATA_TYPE_DCT,\
    RB_CNT, MESSAGE = check_raster_bands(HSI_RASTER_STR)
ORDERED_BANDS2QUERY_LST = sort_bands2keep()

CLOUD_HEADER_DCT = mk_cloud_header_dct(CLOUD_FILE_HEADER_STR)

# Create a linesplit index EX: r,g,b -> 0,1,2
CLOUD_FIELD2KEEP_LST_LINESPLIT_INDEX = cloud_field_filter(
    CLOUD_FILE_FIELDS2KEEP_STR, CLOUD_HEADER_DCT)


CLOUD_FILE_HEADER = mk_cloud_file_header(
    CLOUD_FILE_FIELDS2KEEP_STR, ORDERED_BANDS2QUERY_LST)

RASTER_VAL2SKIP, MESSAGE = sort_values2keep(RASTER_VAL2SKIP_USER)

if __name__ == '__main__':

    # Skipping header line if cloud file
    print('**************** MAIN MULTITHREADING STARTED *********************')
    print('* Monitor system load. Change OPTIMIZATION variables if needed.***')

# =============================================================================
# Check if output files should be overwriten if they exist
# =============================================================================
    if PROTECT_OUTPUT_FILES is True:
        MESSAGE, ERR = output_is_a_file(OUTPUT_CLOUD_FILE_STR)
        if ERR == 1:
            sys.exit(
                'Output file exists! Delete or change output file name,\
                or change the PROTECT_OUTPUT_FILES variable to '"'False'"'')
    elif PROTECT_OUTPUT_FILES is False:
        pass
    else:
        sys.exit('Select True or False for PROTECT_OUTPUT_FILES variable')

    if PROTECT_OUTPUT_FILES is True:
        MESSAGE, ERR = output_is_a_file(OUTPUT_RASTER_FILE_STR)
        if ERR == 1:
            sys.exit(
                'Output file exists! Delete or change output file name,\
                 or change the PROTECT_OUTPUT_FILES variable to '"'False'"'')
    elif PROTECT_OUTPUT_FILES is False:
        pass
    else:
        sys.exit('Select True or False for PROTECT_OUTPUT_FILES variable')

# =============================================================================
#   To write or not to write header... That is the question!
#   Comment this out up to OUTPUT_FILE.close() to skip point cloud creation
# =============================================================================

    if WRITE_HEADER is True:
        MESSAGE, ERR = output_is_a_file(OUTPUT_CLOUD_FILE_STR)
        if OUTPUT_FILE_WRITE_MODE == 'a':
            if ERR == 1:  # 1 == files exits and do not write header
                OUTPUT_FILE = create_output_file(OUTPUT_CLOUD_FILE_STR)

            elif ERR == 0:  # files does not exit
                OUTPUT_FILE = create_output_file(OUTPUT_CLOUD_FILE_STR)
                n = 0
                for field in CLOUD_FILE_HEADER:
                    n += 1
                    OUTPUT_FILE.write(field)
                    # make csv, skip empty lines
                    if n < len(CLOUD_FILE_HEADER):
                        OUTPUT_FILE.write(OUTPUT_FILE_CSV_SEPARATOR)

                    elif n == len(CLOUD_FILE_HEADER):
                        OUTPUT_FILE.write('\n')

        elif OUTPUT_FILE_WRITE_MODE == 'w':
            # File will be overwriten.  Add Header
            OUTPUT_FILE = create_output_file(OUTPUT_CLOUD_FILE_STR)
            n = 0
            for field in CLOUD_FILE_HEADER:
                n += 1
                OUTPUT_FILE.write(field)
                # make csv, skip empty lines
                if n < len(CLOUD_FILE_HEADER):
                    OUTPUT_FILE.write(OUTPUT_FILE_CSV_SEPARATOR)

                elif n == len(CLOUD_FILE_HEADER):
                    OUTPUT_FILE.write('\n')
        else:
            sys.exit(
                'Select "a" (append) or "w" (over Write) \
                    for OUTPUT_FILE_WRITE_MODE variable')

    elif WRITE_HEADER is False:
        OUTPUT_FILE = create_output_file(OUTPUT_CLOUD_FILE_STR)

    else:
        sys.exit('Select True or False for WRITE_HEADER variable')

# =============================================================================
#    Make a nested list of n THREADS * n with n N_POINTS.
# =============================================================================
    GROUPERLST = []
    P = Pool(THREADS)
    n = 0
    cycle = THREADS*N_POINTS*WRITE_CYCLE_MULTIPLIER

    for skips in range(0, LINE2SKIP_IN_CLOUD_FILE):
        skips = CLOUD_LAYER.readline()

    for x in grouper(CLOUD_LAYER, N_POINTS):
        n += 1
        GROUPERLST.append(x)
        if n == THREADS*WRITE_CYCLE_MULTIPLIER:
            print('Sending', cycle, 'points to', THREADS, 'threads.')
            # print len(GROUPERLST)
            # print getsizeof(GROUPERLST)
            # sys.exit()
# =============================================================================
# write lines: This should be write output list not output line
# =============================================================================
            for hsi_3d_fusionlst in P.imap(hsi_3d_fusion, GROUPERLST):
                for lines in hsi_3d_fusionlst:
                    OUTPUT_FILE.write(lines)
                n = 0
                GROUPERLST = []
#   write last lines to files
    print('Final WRITE_CYCLE. Sending last points to', THREADS, 'threads.')
    for hsi_3d_fusionlst in P.imap(hsi_3d_fusion, GROUPERLST):
        for lines in hsi_3d_fusionlst:
            OUTPUT_FILE.write(lines)
        n = 0
        GROUPERLST = []
# =============================================================================
# write lines end
# =============================================================================
    P.close()
    P.join()

#   CLOSE ALL FILES
    RASTER_DS = None
    CLOUD_LAYER.close()
    OUTPUT_FILE.close()

    print(rasterize(CLOUD_FILE_STR, CLOUD_HEADER_DCT, LINE2SKIP_IN_CLOUD_FILE,
                    HSI_RASTER_STR, OUTPUT_RASTER_FILE_STR,
                    OUTPUT_RASTER_NODATA_VAL, HSI_RASTER_MASK_VALUES))


#   Stop Timer
    STOP_TIME = time.time()
    TOTAL_TIME = (STOP_TIME-START_TIME)/60
    print('done:', TOTAL_TIME, 'minutes')
